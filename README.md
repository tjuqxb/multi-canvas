# Multi-Canvas

This is a demo for a Collaboration white board.

### Basic functions:

1 Create/save/open projects  
2 Join an online project in real time collaboration  
3 Basic drawing functions include:  
-- Draw rectangles in various sizes  
-- Move rectangles  
-- Delete rectangles  
4 Supports PC and mobile devices

### Run the app:

```console
-- python simpleHttpServer.py
-- python wsserver.py
```

The app is running at http://localhost:5000/  
To test it on mobile device, please change the file `.\static\js\websocketClient.js` line 1:

```JavaScript
let ws = new WebSocket('ws://localhost:9000');
```

Using the local server IP address instead of `localhost`. The app(static web page) is running at local server port 5000.
